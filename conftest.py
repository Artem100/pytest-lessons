import os
import random
import math

import pytest
from faker import Faker


@pytest.fixture(scope='session')
def generate_dictionary():
    variable_dict = {}
    for i in range(1, 10):
        variable_dict[i] = i
    return variable_dict


@pytest.fixture()
def faker():
    return Faker()


@pytest.fixture(scope='function')
def fixture_that_return_value_and_finalizer(request):
    random_integer = str(random.randint(1, 100))

    def finalaz():
        print("Number was: ", random_integer)
    request.addfinalizer(finalaz)
    return random_integer


class TestClass:

    def __init__(self, mod1, mod2):
        self.mod1 = mod1
        self.mod2 = mod2

    def hello(self, name):
        return f"Hello, {name}"


@pytest.fixture
def fixture_return_class():
    # Через переменные мы можем передавать объекты библиотек и затем вызывать их методы в тесте
    # Также мы можем вызывать метод hello, т.к. это функция класса на который мы делаем return
    return TestClass(mod1=math, mod2=random)



def pytest_addoption(parser):
    parser.addoption(
        "--url",
        action="store",
        default="ya.ru",
        help="This is request url"
    )


@pytest.fixture
def url_param(request):
    return request.config.getoption("--url")


@pytest.fixture(scope='session', autouse=True)
def configure_html_report_env(request,):
    request.config._metadata.update(
        {
        "custom": "Hello world"
        })
    yield


# Логирование начала теста, вызова и окончания
@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    if report.when == 'call':
        # only add this during call instead of during any stage
        report.test_metadata = 'whatever'
        # edit stage metadata
        report.stage_metadata = {
            'foo': 'bar'
        }
    elif report.when == 'setup':
        report.stage_metadata = {
            'hoof': 'doof'
        }
    elif report.when == 'teardown':
        report.stage_metadata = {
            'herp': 'derp'
        }