import pytest

from utils import Utils


@pytest.fixture(scope='function')
def generate_list_6_values():
    generated_list = []
    for i in range(6):
        generated_list.append(i)
    return generated_list


@pytest.fixture(scope='function')
def number_operations():
    return Utils()

@pytest.fixture(scope='function')
def generate_number_1(request):
    # num: Utils().generate_number
    request.cls.num = Utils().generate_number
    yield
    num = 0
    print("\n\n\n\n", num, "\n\n\n")


@pytest.fixture(scope='function')
def generate_number():
    num = Utils().generate_number()
    # def resource_teardown():
    #     num = 0
    # request.addfinalizer(resource_teardown)
    # print("\n\n\n\n", num, "\n\n\n")
    # return num
    yield num
    num = 0
    print("\n\n\n\n", num, "\n\n\n")

@pytest.fixture(params=['a', 'b', 'c', 'd'])
def fixture_with_params(request):
    return request.param


@pytest.fixture(scope='class')
def fixture_class():
    return "Some value from class"