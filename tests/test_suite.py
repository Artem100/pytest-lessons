import random

import pytest


@pytest.fixture(scope='module')
def random_string():
    list_words = ['win', 'lose', 'draw']
    return random.choice(list_words)


def test_01(generate_list_6_values):
    """

    Check that generated list has len 6

    1. Create list from 6 elements in fixture 'generate_list_8_length'
    2. Check that generated list has len = 6

    :param generate_list_6_values:
    """

    assert len(generate_list_6_values) == 7


def test_02(number_operations):
    """

    Operation of sum and check the results

    1. Input two variables with int type values
    2. Call fixture, that contain class with function to operate integers
    3. Call summing numbers method and input variables
    4. Check result of sum

    :param number_operations
    """

    number_1 = 1
    number_2 = 3
    assert number_operations.summing_numbers(number_1, number_2) == 4


def test_03(random_string):
    """

    Check type of returned value from fixture

    1. Call module fixture, that return selected string from list
    2. Check that returned value's type is string

    :param random_string:
    :return:
    """

    assert type(random_string) == str


def test_04(generate_list_6_values, value=5):
    """

    Check value in fixture list

    Set variable and check it in the list

    :param generate_list_6_values:
    :param value:
    :return:
    """

    assert value in generate_list_6_values


def test_05(generate_dictionary):
    """

    Check method dictionary clear

    1. Generate dictionary
    2. Clear dictionary in test
    3. Check that length of dictionary is 0

    :param generate_dictionary:
    """

    generate_dictionary.clear()
    assert len(generate_dictionary) == 0


def test_06(generate_number):
    """

    Generate new int

    Input generated int and check that values doesn't contain comma

    :return:
    """
    string = str(generate_number)
    print(string)
    assert "." or "," in string


def test_07(fixture_that_return_value_and_finalizer):
    """

    Test with functional scope fixture and finalizer


    :param fixture_that_return_value_and_finalizer:
    """
    number = fixture_that_return_value_and_finalizer

    print("Generated number is: ", number)


def test_08(fixture_return_class):
    """

    Try to use fixture, that return object from another class

    :param fixture_return_class:
    :return:
    """
    # В тесте мы обращаемся к фикстуре, к ее объектам которые она возрващает и через них мы вызываем функции
    assert fixture_return_class.mod1.pow(4, 2) == 16
    # pow - возведение в степень
    assert fixture_return_class.mod2.choice(['a', 'b', 'c']) == 'c'
    assert fixture_return_class.hello("Marcus")


def test_09(fixture_with_params):
    """

    Try to use params from fixture only

    :return:
    """
    print(fixture_with_params)


@pytest.mark.parametrize("year", [1990, 1991])
def test_10(fixture_with_params, year):
    """

    Combine data params of test method and fixture

    :param fixture_with_params:
    :return:
    """

    # По алгоритму сначало обрабатываются все данные из фикстуры а потом с тестового метода
    print(year, fixture_with_params)
